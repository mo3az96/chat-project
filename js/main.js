// return messages as array
function getUserMessages(user) {
    return JSON.parse(localStorage.getItem(user));
}
// add message to caht and save it in localstorage
function addUserMessage(parentClass, message) {

    var userMessages = getUserMessages(parentClass + 'arr');
    userMessages.push({
        "message": message
    });
    var newMessages = JSON.stringify(userMessages)
    localStorage.setItem(parentClass + "arr", newMessages);
    $('<div class=divText>' + message + '</div>').appendTo('.' + parentClass + ' .chat');
}

$(document).ready(function () {

    var source = document.querySelector("#test").innerHTML
    // compile it using Handlebars
    var template = Handlebars.compile(source);

    function DisplayUsers(data) {
        fetch("https://5d1e212e3374890014f00c72.mockapi.io/users").then(function (response) {
            // with the response, convert it to JSON, then pass it along
            response.json().then(function (json) {
                // print that JSON
                data(json)
            });
        });
    }




    var base = 'https://5d1e212e3374890014f00c72.mockapi.io';

    // use fetch on the /posts route, then pass the response along
    function fet(url, data) {
        fetch(url).then(function (response) {
            // with the response, convert it to JSON, then pass it along
            response.json().then(function (json) {
                // print that JSON
                data(json)
            });
        });
    }


    var i = 0;



    // $('body').on("click", '.add p', function (e) {
    //     var parentId = $(this).attr("id");
    //     var parentClass = $(this).parent().parent().attr("class");
    //     var div = ".chats." + parentId;

    //     if ($(".getmessage").width() > 910) {
    //         i++;
    //         $(".extra").css("display", "flex");
    //         $(".extra span").text(i);
    //         $(".extra .names").append("<p class=i>" + parentClass + "</p>");
    //         $(".extra .names p").addClass(parentId);
    //         var x = $(".123:last").attr("class");
    //         console.log(x);


    //         $(div).show();
    //         $(div).addClass("123");
    //         $(div + " .chat span").html("");
    //         fet("http://localhost:5050/API/index.php", function (json) {
    //             json.forEach(el => {
    //                 if (el.to_id == 100) {
    //                     $(div + " .chat").append("<span class='to'><span class='nameto'>" + parentClass.charAt(0) + "</span>" + el.msg + "</span>");
    //                 }
    //                 if (el.from_id == 100) {
    //                     $(div + " .chat").append("<span class='from'>" + el.msg + "<span class='namefrom'>" + "M" + "</span>" + "</span>");
    //                 }
    //             });
    //         });
    //         $(div + ' .chat').animate({
    //             scrollTop: $(document).height()
    //         }, 1000);

    //     } else {
    //         $('.chat').niceScroll();
    //         $(div).show();
    //         $(div).addClass("123");
    //         $(div + " .chat span").html("");
    //         fet("http://localhost:5050/API/index.php", function (json) {
    //             json.forEach(el => {
    //                 if (el.to_id == 100) {
    //                     $(div + " .chat").append("<span class='to'><span class='nameto'>" + parentClass.charAt(0) + "</span>" + el.msg + "</span>");
    //                 }
    //                 if (el.from_id == 100) {
    //                     $(div + " .chat").append("<span class='from'>" + el.msg + "<span class='namefrom'>" + "M" + "</span>" + "</span>");
    //                 }
    //             });
    //         });
    //         $(div + ' .chat').animate({
    //             scrollTop: $(document).height()
    //         }, 1000);
    //     }
    // });

    $('body').on("click", '.add p', function (e) {
        var parentId = $(this).attr("id");
        var parentClass = $(this).parent().parent().attr("class");
        var div = ".chats." + parentId;


        $('.chat').niceScroll();
        $(div).show();
        $(div).addClass("123");
        $(div + " .chat span").html("");
        fet("http://localhost:5050/API/index.php", function (json) {
            json.forEach(el => {
                if (el.to_id == 100) {
                    $(div + " .chat").append("<span class='to'><span class='nameto'>" + parentClass.charAt(0) + "</span>" + el.msg + "</span>");
                }
                if (el.from_id == 100) {
                    $(div + " .chat").append("<span class='from'>" + el.msg + "<span class='namefrom'>" + "M" + "</span>" + "</span>");
                }
            });
        });
        $(div + ' .chat').animate({
            scrollTop: $(document).height()
        }, 1000);

    });



    // $('body').on("click", '.names p', function (e) {
    //     var div = $(this).text();
    //     console.log(div);
    //     var diav = $(this).attr("class");
    //     console.log(diav);
    // $(div).show();
    // $(div).addClass("123");
    // $(div + " .chat span").html("");
    // fet("http://localhost:5050/API/index.php", function (json) {
    //     json.forEach(el => {
    //         if (el.to_id == 100) {
    //             $(div + " .chat").append("<span class='to'><span class='nameto'>" + parentClass.charAt(0) + "</span>" + el.msg + "</span>");
    //         }
    //         if (el.from_id == 100) {
    //             $(div + " .chat").append("<span class='from'>" + el.msg + "<span class='namefrom'>" + "M" + "</span>" + "</span>");
    //         }
    //     });
    // });
    // $(div + ' .chat').animate({
    //     scrollTop: $(document).height()
    // }, 1000);

    // });

    DisplayUsers(function (json) {
        var getnames = {};
        getnames.names = [];
        json.forEach(element => {
            getnames.names.push({
                firstchar: element.name.charAt(0),
                name: element.name,
                id: element.id
            })
        });
        $(".getnames").html(template(getnames));
    });
    DisplayUsers(function (json) {
        var getmessage = {};
        getmessage.chats = [];
        json.forEach(element => {
            getmessage.chats.push({
                name: element.name,
                id: element.id
            })
        });
        $(".getmessage").html(template(getmessage));
    });




    // create context




    $(".getnames").niceScroll();




    $("body").on("click", ".send", function () {
        var parentClass = $(this).parent().attr('class');
        var message = $("." + parentClass + " .message").val();
        $("." + parentClass + " .chat").append("<span class='from'>" + message + "<span class='namefrom'>" + "M" + "</span>" + "</span>");
        $("." + parentClass + " .message").val("");
        $("." + parentClass + " .chat").animate({
            scrollTop: $(document).height()
        }, 1000);
    });
    $("body").on("click", ".head", function () {
        $(this).parent().toggleClass('inscreen');
        $(this).parent().toggleClass('outscreen');
    });
    $("body").on("click", ".close", function (e) {
        e.stopPropagation();
    });

    $("body").on("click", ".close", function () {

        $(this).parents('.chats').hide();
        $(this).parents('.chats').find('.chat').html("");
    });

});



window.onload = function () {

    $(".lds-facebook").fadeOut(5000, function () {
        $(this).parent().fadeOut();
    });

}